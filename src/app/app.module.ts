import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { AboutPage } from '../pages/about/about';
import { SearchPage } from '../pages/search/search';
import { UserPage } from '../pages/user/user';

import { ModalModule } from 'ngx-bootstrap/modal';


import { QRScanner } from '@ionic-native/qr-scanner';
import { UserProvider } from '../providers/user/user';

import { AngularFireModule } from 'angularfire2';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabaseModule } from 'angularfire2/database';


export const firebaseConfig = {
	
		apiKey: "AIzaSyDO0UUsgGX3NXKdOOsrjl6nOJIC6gFOiOI",
    authDomain: "converce-dev.firebaseapp.com",
    databaseURL: "https://converce-dev.firebaseio.com",
    projectId: "converce-dev",
    storageBucket: 'converce-dev.appspot.com',
    messagingSenderId: "663490379659"
	
};


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    AboutPage,
    SearchPage,
    UserPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    ModalModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    AboutPage,
    SearchPage,
    UserPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    QRScanner,
    UserProvider,
    AngularFireAuth
  ]
})
export class AppModule {}
