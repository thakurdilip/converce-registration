import { Component, Input, TemplateRef, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Navbar } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { Md5 } from 'ts-md5/dist/md5';
import { HomePage } from '../home/home';
import { UserPage } from '../user/user';

/**
 * Generated class for the SearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class SearchPage {
  @ViewChild(Navbar) navBar: Navbar;
  @Input() edit = false;
  users: any[];
  newlySearchedUsers;
  modalRef: BsModalRef;

  nameCheck: any;

  modalContent = {
    printUser: false,
    editOrNew: 'edit',
    user: {
      name: '',
      email: '',
      phone: '',
      lastname: '',
      'l-name': "",
      uid:<any> ''
    }
  }

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private modalService: BsModalService,
              public usrSrv: UserProvider) {

                this.usrSrv.getAllUsers().subscribe( (users) => {
                  this.users = users;
                  console.log("User details = ", this.users);
                  this.newlySearchedUsers = users.slice(0,15);
                  this.cleanUp();
                })
  }

  ngOnInit() {

    this.usrSrv.getAllUsers().subscribe( (users) => {
      this.users = users;
      console.log("User details = ", this.users);
      this.newlySearchedUsers = users.slice(0,15);
      this.cleanUp();
    })

  }

  ionViewDidLoad() {
    this.setBackButtonAction()
    console.log('ionViewDidLoad SearchPage');
  }
  setBackButtonAction(){
    this.navBar.backButtonClick = () => {
    //Write here wherever you wanna do
       this.navCtrl.pop({
         animate:false,
         direction: 'back'
       })
    }
 }
  // Cleans up some of the data, capitalization, constant data schema etc.
  cleanUp(){
    this.users.forEach(user => {
      user.name = this.styleName(user.name)
      if(user['l-name']){
        user.lastname = this.styleName(user['l-name']);
      } else {
        user.lastname = "";
      }

      if(user.company){
        user.company = this.styleName(user.company);
      } else {
        user.company = "";
      }
    })
  }

  styleName(name){
    let result = ""
    let arr = name.split(" ");

    for(let i = 0; i < arr.length; i++){
      result += arr[i].toUpperCase().charAt(0) + arr[i].slice(1).toLowerCase();
      if(i < arr.length) {
        result += " "
      }
    }

    return result;
  }

  searchByEmail($event) {
    // this.newlySearchedUsers = this.users;
    let q = $event.target.value.toLowerCase();

    let result = [];
    console.log("Users in search = ", this.users)

    this.users.forEach(user => {
      // debugger;
      // Some users may not have last name or company saved, so need to build this checker
      let emailCheck = user.email && user.email.toLowerCase().includes(q);
      if(user.lastname){
        this.nameCheck = (user.name || user.lastname) && (user.name.toLowerCase() + user.lastname.toLowerCase()).includes(q);
        // console.log(user.name.toLowerCase() + ' ' + user.lastname.toLowerCase());
      }
      else {
        this.nameCheck = (user.name || user.lastname) && (user.name.toLowerCase()).includes(q);
      }
      let companyCheck = user.company && user.company.toLowerCase().includes(q);

      if (emailCheck || this.nameCheck || companyCheck){
        result.push(user);
        }
      });

    result = result.slice(0, 15);

    this.newlySearchedUsers = result;
  }

  checkin(user){
    console.log("Checkin ", user)
    // this.router.navigate([`checkin/${this.deviceid}/search/user/${user.$key}`]);
    this.navCtrl.push(UserPage, {
      data: user
    },{animate:false})
  }

  openModal(template: TemplateRef<any>, type, user) {
    this.modalContent.printUser = false;
    this.modalContent.editOrNew = type;

    if(user){
      this.modalContent.user = user;
      this.modalContent.user.uid = user.$key;
    } else {
      this.modalContent.user = {
        name: '',
        email: '',
        phone: '',
        lastname: '',
        uid:<any> '',
        'l-name': ''
      }
    }

    this.modalRef = this.modalService.show(template);
  }

  editUser(){
    let that = this;

    this.modalContent.user['l-name'] = this.modalContent.user.lastname;
    if(this.modalContent.editOrNew == 'New User') {
      this.usrSrv.saveNormalUser(this.modalContent.user)
      this.modalContent.user.uid =  Md5.hashStr(this.modalContent.user.email);;
    } else {
      this.usrSrv.saveUser(this.modalContent.user);
      console.log(this.modalContent.user)
    }

    this.modalContent.printUser = true;
  }

  goToPrint(uid){
    this.modalRef.hide();
    // this.router.navigate(['/checkin/walk-in/' + this.modalContent.user.uid]);
  }

  goToQrScan(){
    // this.router.navigate([`/checkin/${this.deviceid}/qr`]);
    this.navCtrl.push(HomePage,{},{animate:false});
  }

}
