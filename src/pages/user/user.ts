import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { UserProvider } from '../../providers/user/user';
import { HomePage } from '../home/home';

/**
 * Generated class for the UserPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-user',
  templateUrl: 'user.html',
})
export class UserPage {
  userData: any;
  deviceid= 6;
  checkedIn = false;
  name: string = '';


  constructor(public navCtrl: NavController, public navParams: NavParams, public userSrv: UserProvider) {
    this.userData = navParams.get('data');
    console.log("Data key = ", this.userData.$key)
    console.log("Data uid = ", this.userData.uid)
    console.log("Data here in search = ",this.userData.lastname)
    console.log("Data here in search2 = ",this.userData['l-name'])
    this.name = this.userData['name'] + " " + this.userData['l-name'] || this.userData['lastname'];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UserPage');
  }

  confirm(){
    console.log("Confirm in user.ts new one = ", this.userData.name)
    if (this.userSrv.checkin(this.userData)) {
      this.userSrv.checkinByPrinter(this.userData, this.deviceid);

      setTimeout(() => {
        this.navCtrl.push(HomePage,{},{animate:false});
      }, 500)
      // this.navCtrl.push(HomePage,{},{animate:false})
    }
    else {
      console.log("Couldn't confirm checkin")
    }
    // debugger
    // this.userSrv.checkinByPrinter(this.userData, this.deviceid).then((user) => {

    //   console.log(user);
    //   if(this.cameFromAdmin){
    //     // this.sendText();
    //     // this.print();
    //     // this.sendEmail();
    //     setTimeout(() => {
    //       this.router.navigate(['/admin/walkin']);
    //     }, 5000);
    //   } else {
    //     // this.sendText();
    //     // this.print();
    //     // this.sendEmail();
    //     setTimeout(() => {
    //       this.router.navigate([`/checkin/${this.deviceid}/qr`]);
    //     }, 5000);
    //   }
    // })
  }

  goBack(){
    this.navCtrl.push(HomePage,{},{animate:false})
  }

}
