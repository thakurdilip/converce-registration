import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { HomePage } from '../home/home';

import { UserProvider } from '../../providers/user/user'

import { FirebaseListObservable } from 'angularfire2/database';

/**
 * Generated class for the AboutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-about',
  templateUrl: 'about.html',
})
export class AboutPage {
  public users: any[];

  constructor(public navCtrl: NavController, public navParams: NavParams, public platform: Platform, public user: UserProvider) {
    // platform.registerBackButtonAction(function(event) {
    //   this.home(); =
    // }, 101);
    this.user.getAllUsers().subscribe(user => {
      this.users = user;
      console.log("Users", this.users)
    })

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AboutPage');
    console.log("Users = ", this.user.getAllUsers())
  }

  home() {
    this.navCtrl.push(HomePage,{},{animate:false});
  }

}
