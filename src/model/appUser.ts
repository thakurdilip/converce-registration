export interface AppUser{
    uid?: string;
    email: null;
    name: null;
    phone: null;
    company: null;
    isAdmin?: boolean;
}