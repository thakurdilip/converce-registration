// import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';

import { Md5 } from 'ts-md5/dist/md5';
import { AppUser } from '../../model/appUser';

/*
  Generated class for the UserProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserProvider {
  path = 'events/Mumbai_Regulatory_Summit_2019_4832'
  uid: any;
  
  constructor(private db: AngularFireDatabase) {
    console.log('Hello UserProvider Provider');
  }

  getAllUsers(){
    return this.db.list(this.path + '/users');
  } 

  get(uid: string): FirebaseObjectObservable<AppUser>{
    return this.db.object(this.path + '/users/' + uid);
  }

  saveNormalUser(user){
    let userEmail = user.email;
    let hash = Md5.hashStr(userEmail);
    return this.db.object(this.path + '/users/' + hash).update(user);
  }

  searchByEmail(start, end): FirebaseListObservable<any>{
    return this.db.list(this.path + '/users/', {
      query: {
        orderByChild: 'email',
        startAt: start,
        endAt: end
      }
    });
  }

  saveUser(user){
    return this.db.object(this.path + '/users/' + user.uid).update(user);
  }

  checkin(user){
    console.log("user in service = ", user.uid)
    console.log("user in service = ", user.$key)
    let key = user.uid || user.$key;
    return this.db.object(this.path + '/users/' + key).update({
      checkedin: true
    });
  }

  checkinByPrinter(user, printerId){
    console.log("checkin by printer")
    user.uid = user.$key;
    const printerUserRef= this.db.list(this.path + '/checkinByPrinter/' + printerId);
    return printerUserRef.push(user);
  }

}
